/*
*
*	Libraries.
*
*/
const { ArgumentType } = require('discord.js-commando');
const { NewsFeed } = require('../libs/cso/news');
const { isEmpty, isEqual } = require('lodash');


/*
*
*	Exports.
*
*/
module.exports = class RegionChannelArgumentType extends ArgumentType {
	constructor(client) {
		super(client, 'regionchannel');
	}

	validate(val) {
		val = val.trim().toLowerCase();
		return isEmpty (val) || isEqual (val, '*') || isEqual (val, 'all') || NewsFeed.hasNews (val);
	}

	parse(val) {
		return val.trim().toLowerCase();
	}
};
