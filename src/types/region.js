/*
*
*	Libraries.
*
*/
const { ArgumentType } = require('discord.js-commando');
const { NewsFeed } = require('../libs/cso/news');


/*
*
*	Exports.
*
*/
module.exports = class RegionArgumentType extends ArgumentType {
	constructor(client) {
		super(client, 'region');
	}

	validate(val) {
		val = val.trim().toLowerCase();
		return NewsFeed.hasNews (val);
	}

	parse(val) {
		return val.trim().toLowerCase();
	}
};
