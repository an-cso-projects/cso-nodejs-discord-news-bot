/*
*
*	Libraries.
*
*/
const { Command } = require('discord.js-commando');
const { NewsFeed } = require('../../libs/cso/news');
const { keys } = require('lodash');


/*
*
*	Exports.
*
*/
module.exports = class NewsDelCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'del',
			group: 'news',
			memberName: 'del',
			description: 'Removes this channel from a news feed subscription.',
			examples: ['del steam'],
			guildOnly: true,
			clientPermissions: ['VIEW_CHANNEL', 'SEND_MESSAGES'],
			userPermissions: ['ADMINISTRATOR'],
			throttling: {
				usages: parseInt (process.env.DISCORD_COMMAND_DEL_THROTTLING_USAGE),
				duration: parseInt (process.env.DISCORD_COMMAND_DEL_THROTTLING_DURATION),
			},

			args: [
				{
					key: 'region',
					label: 'region',
					prompt: `What region would you like to unsubscribe? **${keys (NewsFeed.news)}**`,
					type: 'region',
				},
			],
		});
	}


	async run(msg, args) {
		const region = args.region;
		const guild = msg.guild;
		const channel = msg.channel;
		const news = NewsFeed.news[region];
		const result = await news.deleteSubscriber (guild, channel);

		if (!result) return msg.say (`${channel} is already unsubscribed \`${region}\` news feed.`);

		// Tells the user in current channel.
		return msg.say (`Removed ${channel} from \`${region}\` news feed.`);
	}
};
