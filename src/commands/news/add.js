/*
*
*	Libraries.
*
*/
const { Command } = require('discord.js-commando');
const { NewsFeed } = require('../../libs/cso/news');
const { keys, isEmpty } = require('lodash');


/*
*
*	Exports.
*
*/
module.exports = class NewsAddCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'add',
			group: 'news',
			memberName: 'add',
			description: 'Adds this channel into a news feed subscription.',
			examples: ['add steam'],
			guildOnly: true,
			clientPermissions: ['VIEW_CHANNEL', 'SEND_MESSAGES'],
			userPermissions: ['ADMINISTRATOR'],
			throttling: {
				usages: parseInt (process.env.DISCORD_COMMAND_ADD_THROTTLING_USAGE),
				duration: parseInt (process.env.DISCORD_COMMAND_ADD_THROTTLING_DURATION),
			},

			args: [
				{
					key: 'region',
					label: 'region',
					prompt: `What region would you like to subscribe? **${keys (NewsFeed.news)}**`,
					type: 'region',
				},
				{
					key: 'printNews',
					label: 'printNews',
					prompt: 'Would you like to post current news in this channel now? **(true = yes, false = no)**',
					type: 'boolean',
				},
			],
		});
	}


	async run(msg, args) {
		const region = args.region;
		const guild = msg.guild;
		const channel = msg.channel;
		const news = NewsFeed.news[region];
		const addresult = await NewsFeed.news[region].addSubscriber (guild, channel);

		if (!addresult) { return msg.say (`${channel} already subscribed \`${region}\` news feed.`); }

		// Tells the user in current channel.
		let result = msg.say (`Added ${channel} into \`${region}\` news feed.`);

		if (args.printNews) {
			const cache = await news.retrieveCache();
			if (isEmpty(cache) || isEmpty(cache.articles)) {
				return msg.say (`Sorry. \`${region}\` news feed is not ready yet.`);
			}

			msg.say ('Printing the articles ...');
			for (const article of cache.articles) {
				result = await news.send(channel, article);
			}
			msg.say ('Printing the articles ...Done');
		}

		return result;
	}
};
