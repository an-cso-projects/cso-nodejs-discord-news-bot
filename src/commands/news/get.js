/*
*
*	Libraries.
*
*/
const { Command } = require('discord.js-commando');
const { NewsFeed } = require('../../libs/cso/news');
const { keys, isEmpty } = require('lodash');


/*
*
*	Exports.
*
*/
module.exports = class NewsGetCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'get',
			group: 'news',
			memberName: 'get',
			description: 'Gets cached news from a news feed subscription.',
			examples: ['get steam'],
			guildOnly: true,
			clientPermissions: ['VIEW_CHANNEL', 'SEND_MESSAGES'],
			userPermissions: ['ADMINISTRATOR'],
			throttling: {
				usages: parseInt (process.env.DISCORD_COMMAND_GET_THROTTLING_USAGE),
				duration: parseInt (process.env.DISCORD_COMMAND_GET_THROTTLING_DURATION),
			},

			args: [
				{
					key: 'region',
					label: 'region',
					prompt: `What region would you like to retrieve? **${keys (NewsFeed.news)}**`,
					type: 'region',
				},
			],
		});
	}


	async run(msg, args) {
		const region = args.region;
		const news = NewsFeed.news[region];
		const channel = msg.channel;

		// Tells the user in current channel.
		const cache = await news.retrieveCache();
		if (isEmpty(cache) || isEmpty(cache.articles)) {
			return msg.say (`Sorry. \`${region}\` news feed is not ready yet.`);
		}

		msg.say(`Getting \`${region}\` cached articles...`);
		for (const article of cache.articles) {
			await news.send(channel, article);
		}
		return msg.say(`Getting \`${region}\` cached articles...Done`);
	}
};
