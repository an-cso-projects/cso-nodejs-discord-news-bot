/*
*
*	Libraries.
*
*/
const Commando = require('discord.js-commando');
const _ = require('lodash');


/*
*
*	Exports.
*
*/
module.exports = class UtilGuildsCommand extends Commando.Command {
	constructor(client) {
		super(client, {
			name: 'guilds',
			group: 'util',
			memberName: 'guilds',
			description: 'Gets connected guilds.',
			ownerOnly: true,
			clientPermissions: ['VIEW_CHANNEL', 'SEND_MESSAGES'],
		});
	}


	run(msg) {
		let strBuf = 'Connected guilds:';
		_.forEach (msg.client.guilds.array(), (guild) => {
			strBuf += '\n';
			strBuf += `> (${guild.id}) ${guild}`;
		});
		return msg.direct(`${strBuf}`);
	}
};
