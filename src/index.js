/*
*
*	Libraries.
*
*/
require('dotenv').config();
const Winston = require('winston');
const Commando = require('discord.js-commando');
const Path = require('path');
const oneLine = require('common-tags').oneLine;
const Sequelize = require('sequelize');
const SequelizeProvider = require('./libs/providers/sequelize');
const { NewsFeed, NexonNews, SteamNews, BeanfunNews, TiancityNews } = require('./libs/cso/news');


// Prepares the logger.
Winston.configure({
	level: process.env.LOGGER_LEVEL,
	format: Winston.format.combine(
		Winston.format.colorize(),
		Winston.format.timestamp({
			format: 'YYYY-MM-DD HH:mm:ss',
		}),
		Winston.format.errors({ stack: true }),
		Winston.format.splat(),
		Winston.format.simple(),
	),
	exitOnError: false,
	silent: false,
	transports: [
		new Winston.transports.Console({ handleExceptions: true }),
	],
});


// Creates a bot.
const client = new Commando.Client({
	owner: process.env.DISCORD_OWNER_ID,
	commandPrefix: 'cso>',
});


// Creates persistent data.
client.setProvider(
	new SequelizeProvider(new Sequelize({
		logging: false,
		native: true,
		dialect: 'sqlite',
		storage: Path.join(__dirname, 'database.sqlite3'),
	})),
).catch(Winston.error);


// Initializes the news feed.
NewsFeed.client = client;
NewsFeed.news = new NexonNews(client);
NewsFeed.news = new SteamNews(client);
NewsFeed.news = new BeanfunNews(client);
NewsFeed.news = new TiancityNews(client);


// Registers bot's hooks.
client
	.on('error', Winston.error)
	.on('warn', Winston.warn)
	.on('debug', Winston.debug)
	.on('ready', () => {
		Winston.info(`Client ready; logged in as ${client.user.username}#${client.user.discriminator} (${client.user.id})`);
	})
	.on('disconnect', () => { Winston.warn('Disconnected!'); })
	.on('reconnecting', () => { Winston.warn('Reconnecting...'); })
	.on('commandError', (cmd, err) => {
		if(err instanceof Commando.FriendlyError) return;
		Winston.error(`Error in command ${cmd.groupID}:${cmd.memberName}`, err);
	})
	.on('commandBlocked', (msg, reason) => {
		Winston.info(oneLine`
			Command ${msg.command ? `${msg.command.groupID}:${msg.command.memberName}` : ''}
			blocked; ${reason}
		`);
	})
	.on('commandPrefixChange', (guild, prefix) => {
		Winston.info(oneLine`
			Prefix ${prefix === '' ? 'removed' : `changed to ${prefix || 'the default'}`}
			${guild ? `in guild ${guild.name} (${guild.id})` : 'globally'}.
		`);
	})
	.on('commandStatusChange', (guild, command, enabled) => {
		Winston.info(oneLine`
			Command ${command.groupID}:${command.memberName}
			${enabled ? 'enabled' : 'disabled'}
			${guild ? `in guild ${guild.name} (${guild.id})` : 'globally'}.
		`);
	})
	.on('groupStatusChange', (guild, group, enabled) => {
		Winston.info(oneLine`
			Group ${group.id}
			${enabled ? 'enabled' : 'disabled'}
			${guild ? `in guild ${guild.name} (${guild.id})` : 'globally'}.
		`);
	})
	.on('providerReady', () => {
		Winston.info('Provider is ready.');
		NewsFeed.startTracking (process.env.NEWS_UPDATE_INTERVAL);
	});


// Registers bot's commands.
client.registry
	.registerGroup('news', 'CSO News')
	.registerDefaults()
	.registerTypesIn(Path.join(__dirname, 'types'))
	.registerCommandsIn(Path.join(__dirname, 'commands'));


// Runs the bot.
client.login(process.env.DISCORD_BOT_TOKEN);