/*
*
*	Libraries.
*
*/


/*
*
*	Exports.
*
*/
module.exports = class ArticleComparator {
	static isEquals(a, b) { return ArticleComparator.byTitle(a, b) && ArticleComparator.byContent(a, b); }
	static byTitle(a, b) { return (!b) ? a.title : a.title == b.title; }
	static byDate(a, b) { return (!b) ? a.date : a.date == b.date; }
	static byAuthor(a, b) { return (!b) ? a.author : a.author == b.author; }
	static byUrl(a, b) { return (!b) ? a.url : a.url == b.url; }
	static byIcon(a, b) { return (!b) ? a.icon : a.icon == b.icon; }
	static byThumbnail(a, b) { return (!b) ? a.thumbnail : a.thumbnail == b.thumbnail; }
	static byContent(a, b) { return (!b) ? a.content : a.content == b.content; }
};
