/*
*
*	Libraries.
*
*/
const nodeURL = URL;
const URLParse = require('url').parse;
const URLFormat = require('url').format;


/*
*
*	Exports.
*
*/
module.exports = class URL {
	static repairUrl(link, base) {
		try {
			const myUrl = new nodeURL(link, base);
			if (!myUrl.protocol) myUrl.protocol = 'https';
			return myUrl.href;
		}
		catch (_e) {
			try {
				link = URLParse(link);
				if (!link.protocol) link.protocol = 'https';
				return URLFormat(link);
			}
			catch (_ee) {
				return link;
			}
		}
	}
};
