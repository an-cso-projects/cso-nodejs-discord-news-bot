module.exports = {
	NewsFeed: require('./newsfeed'),
	News: require('./base'),
	MarkdownNews: require('./markdown'),
	NexonNews: require('./news/nexon'),
	SteamNews: require('./news/steam'),
	BeanfunNews: require('./news/beanfun'),
	TiancityNews: require('./news/tiancity'),
};
