/*
*
*	Libraries.
*
*/
const Winston = require('winston');
const News = require('./base');
const { CommandoClient } = require('discord.js-commando');
const { keys, indexOf, forEach, isString, isObjectLike, isEmpty, chunk, values } = require('lodash');


/**
 * News feed.
 */
class NewsFeed {

	/**
	 * The client the news feed is for.
	 *
	 * @type {CommandoClient}
	 */
	static get client() {
		return this._client;
	}
	static set client(client) {
		this._client = client;
	}


	/**
	 * Available news.
	 *
	 * @type {Object.<string, News>}
	 * @throws Invalid news object.
	 */
	static get news() {
		return this._news || {};
	}
	static set news(news) {
		if (news instanceof News === false) {
			throw new TypeError('Invalid news.');
		}

		if (!isObjectLike(this._news)) this._news = {};
		this._news[news.name] = news;
	}


	/**
	 * Checks whether this news is registered.
	 *
	 * @param {News|string} news The news to check.
	 */
	static hasNews(news) {
		news = isString(news) ? news : news.name;
		news = news.trim().toLowerCase();
		return indexOf(keys(this.news), news) !== -1;
	}


	/**
	 * Starts tracking all registered news updates.
	 *
	 * @param {number} delay Update time interval in minute(s).
	 */
	static startTracking(delay) {
		if (this._timer) {
			Winston.warn('startTracking() Is already running.', { class: 'NewsFeed', status: 'warn' });
			return;
		}
		const milidetik = delay * 60 * 1000;

		Winston.debug('startTracking()', { class: 'NewsFeed', status: 'start' });

		const self = this;
		const twoExecAtOneTime = async () => {
			for (const moreNews of chunk(values(self.news), parseInt(process.env.NEWS_MAX_EXEC_PER_QUEUE))) {
				const funcs = [];
				for (const oneNews of moreNews) {
					funcs.push(self.updateNews (oneNews, true));
				}
				await Promise.allSettled(funcs);
			}
		};
		this._timer = setInterval(twoExecAtOneTime, milidetik);
		twoExecAtOneTime();

		Winston.verbose('startTracking()', { class: 'NewsFeed', status: 'done' });
	}


	/**
	 * Halts tracking all registered news updates.
	 */
	static stopTracking() {
		if (this._timer) {
			Winston.warn('startTracking() Is not running.', { class: 'NewsFeed', status: 'warn' });
			return;
		}

		Winston.debug('stopTracking()', { class: 'NewsFeed', status: 'start' });
		clearInterval (this._timer);
		this._timer = null;
		Winston.verbose('stopTracking()', { class: 'NewsFeed', status: 'done' });
	}


	/**
	 * Updates a news.
	 *
	 * @param {News|string} news The news to update.
	 * @param {boolean} sendMsg Broadcasts new article messages to all subscribers.
	 */
	static async updateNews(news, sendMsg) {
		if (this.client instanceof CommandoClient === false) throw new TypeError('Invalid client.');
		news = isString(news) ? news : news.name;
		news = news.trim().toLowerCase();
		if (!this.hasNews(news)) throw new Error('News is not registered.');
		Winston.debug('updateNews() Checking.', { class: 'NewsFeed', status: 'start', news: news });

		const instance = this.news[news];
		const articles = await instance.update();
		if (sendMsg) {
			const self = this;
			for (const guild of self.client.guilds.array()) {
				if (isEmpty(articles)) {
					Winston.info('updateNews() No new articles.', { class: 'NewsFeed', status: 'done', news: news });
					break;
				}

				if (!await instance.hasSubscriber (guild)) {
					Winston.debug('updateNews() Not a subscriber.', { class: 'NewsFeed', status: 'warn', news: news, guild: guild.name });
					continue;
				}

				forEach (await instance.getSubscriber (guild), function(channelId) {
					const channel = self.client.channels.get (channelId);
					if (channel) {
						Winston.debug('updateNews() Sending messages.', { class: 'NewsFeed', status: 'start', news: news, guild: guild.name, channel: channelId, count: articles.length });
						forEach (articles, async function(article) { await instance.send(channel, article); });
						Winston.verbose('updateNews() Sending messages.', { class: 'NewsFeed', status: 'done', news: news, guild: guild.name, channel: channelId, count: articles.length });
					}
					else {
						Winston.verbose('updateNews() Deleting a broken channel.', { class: 'NewsFeed', status: 'start', news: news, guild: guild.name, channel: channelId });
						instance.deleteSubscriber (guild, channelId);
						Winston.verbose('updateNews() Deleting a broken channel.', { class: 'NewsFeed', status: 'done', news: news, guild: guild.name, channel: channelId });
					}
				});
			}
		}

		Winston.verbose('updateNews() Checking.', { class: 'NewsFeed', status: 'done', news: news });
	}
}


module.exports = NewsFeed;
