/*
*
*	Libraries.
*
*/
const News = require('./base');
const { replace } = require('lodash');
const TurndownService = require('turndown');


/**
 * Markdown News.
 * @abstract
 */
class MarkdownNews extends News {

	toRichEmbedMessage(article) {
		article.content = replace (article.content, /\n/g, '<br/>');
		article.content = (new TurndownService()).turndown (article.content);
		return super.toRichEmbedMessage(article);
	}
}


module.exports = MarkdownNews;
