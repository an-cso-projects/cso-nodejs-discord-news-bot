/*
*
*	Libraries.
*
*/
const MarkdownNews = require('../markdown');
const { get, all } = require('axios').default;
const { decode } = require('iconv-lite');
const { load } = require('cheerio');
const { sortBy, uniqWith, includes, slice, differenceWith } = require('lodash');
const { URL, Comparator } = require('../../utils');


/**
 * Tiancity (China) News.
 */
class TiancityNews extends MarkdownNews {
	constructor(client) {
		super(client, 'tiancity');
	}


	async isOutdated() {
		let result = [];
		const self = this;

		return all ([
			get (process.env.TIANCITY_NEWS_SYSTEM_URL, { responseType: 'arraybuffer' }),
			get (process.env.TIANCITY_NEWS_UPDATE_URL, { responseType: 'arraybuffer' }),
			get (process.env.TIANCITY_NEWS_EVENT_URL, { responseType: 'arraybuffer' }),
		])
			.then ((responses) => {
				for (const response of responses) {
					const $ = load(decode(response.data, 'gb2312'));
					$('li', '.newslists').each((_id, elm) => {
						result.push ({
							title: `【${$('.news_classify', 'span', elm).text()}】 ${$('.news_title', 'span', elm).text()}`,
							date: new Date ($('em', elm).text()),
						});
					});
				}
			})
			.then (async () => {
				const cache = await self.retrieveCache();
				result = sortBy (result, Comparator.Article.byDate);
				result = slice (result, Math.max (0, result.length - parseInt(process.env.TIANCITY_NEWS_ARTICLE_LIMIT)));
				return differenceWith (result, cache.articles, Comparator.Article.byTitle).length > 0;
			});
	}


	async retrieve(asJson) {
		let result = [];
		const links = [];

		return all([
			get (process.env.TIANCITY_NEWS_SYSTEM_URL, { responseType: 'arraybuffer' }),
			get (process.env.TIANCITY_NEWS_UPDATE_URL, { responseType: 'arraybuffer' }),
			get (process.env.TIANCITY_NEWS_EVENT_URL, { responseType: 'arraybuffer' }),
		])
			.then((responses) => {
				const baseUrl = process.env.TIANCITY_NEWS_SYSTEM_URL;
				for (const response of responses) {
					const $ = load(decode(response.data, 'gb2312'));
					$('li', '.newslists').each((_id, elm) => {
						const title = `【${$('.news_classify', 'span', elm).text()}】 ${$('.news_title', 'span', elm).text()}`;
						const link = URL.repairUrl($('.news_title', 'span', elm).attr('href'), baseUrl);
						const date = /\[([0-9-]+)\]/.exec ($('em', elm).text())[1];

						links.push (link);
						result.push ({
							title: title,
							date: new Date (date),
							author: process.env.TIANCITY_NEWS_AUTHOR,
							url: link,
							icon: process.env.TIANCITY_NEWS_ICON_URL,
							thumbnail: process.env.TIANCITY_NEWS_THUMBNAIL_URL,
						});
					});
				}
			})
			.then(async () => {
				for (let index = 0; index < links.length; index++) {
					const baseUrl = links[index];
					let $ = load(`<a href=${baseUrl}>${baseUrl}</a>`);
					let contents = $('*');
					if (includes(baseUrl, 'csol.tiancity.com')) {
						const response = await get (baseUrl, { responseType: 'arraybuffer' });
						$ = load(decode(response.data, 'gb2312'));
						contents = $('div.newsshow', 'div.c').remove('h2').remove('div.main.news-detail');
					}
					else if (includes(baseUrl, 'news.17173.com')) {
						const response = await get (baseUrl, { responseType: 'arraybuffer' });
						$ = load(response.data);
						contents = $('div#mod_article', 'div.gb-final-pn-article').remove('a[href*=\'17173.com\']').remove('script');
					}
					$('img[src]', contents).each((id, elm) => {
						const src = URL.repairUrl ($(elm).attr('src'), baseUrl);
						if (id === 0) result[index].thumbnail = src;
						$(elm).replaceWith(src);
					});
					$('a', contents).each((_id, elm) => {
						$(elm).attr('href', URL.repairUrl ($(elm).attr('href'), baseUrl));
					});

					// Now, inserts into articles's content.
					result[index].content = contents.html() || '';
				}

				result = uniqWith (result, Comparator.Article.isEquals);
				result = sortBy (result, Comparator.Article.byDate);
				result = slice (result, Math.max (0, result.length - parseInt(process.env.TIANCITY_NEWS_ARTICLE_LIMIT)));
				if (asJson) result = JSON.stringify (result);
				return result;
			});
	}
}


module.exports = TiancityNews;
