/*
*
*	Libraries.
*
*/
const Winston = require('winston');
const { RichEmbed, Guild, Channel, TextChannel } = require('discord.js');
const { isArray, isEmpty, indexOf, pull, truncate, differenceWith } = require('lodash');
const { brotliCompressSync, brotliDecompressSync } = require('zlib');
const { createHash } = require('crypto');
const { Comparator } = require('../utils');


/**
 * News.
 * @abstract
 */
class News {
	/**
	 * A news article.
	 *
	 * @typedef {Object} NewsArticle
	 * @property {string} title - Title of this article.
	 * @property {string|number|Date} date - The article date.
	 * @property {string} author - The article author.
	 * @property {string} url - The article source url link.
	 * @property {string} icon - The article author icon image url link.
	 * @property {string} thumbnail - The article thumbnail image url link.
	 * @property {string} content - The article content(s).
	 */


	/**
	 * A news cache.
	 *
	 * Articles are stored and compressed with Brolit compression.
	 *
	 * @typedef {Object} NewsCache
	 * @property {string} hash - The MD5 hash of original news articles JSON string.
	 * @property {string} articles - The compressed news articles JSON string.
	 */


	/**
	 * Updating status.
	 *
	 * @name News#updating
	 * @type {boolean}
	 * @readonly
	 */
	get updating() {
		return this._updating;
	}


	/**
	 * @param {CommandoClient} client - The client the news is for.
	 * @param {string} name - Name of this news.
	 */
	constructor(client, name) {
		if(this.constructor.name === 'News') throw new Error(`The base ${this.constructor.name} cannot be instantiated.`);

		/**
		 * Client that this news is for.
		 *
		 * @name News#client
		 * @type {CommandoClient}
		 * @readonly
		 */
		Object.defineProperty(this, 'client', { value: client });

		/**
		 * Name of this news.
		 *
		 * @name News#name
		 * @type {string}
		 * @readonly
		 */
		Object.defineProperty(this, 'name', { value: name.trim().toLowerCase() });
	}


	/**
	 * Gets the latest articles.
	 *
	 * @param {?boolean} [asJson] Returns as JSON string.
	 * @return {Promise<Array<NewsArticle|string>>}
	 * @abstract
	 */
	async retrieve(asJson) {
		throw new Error(`${this.constructor.name} doesn't have a retrieve() method.`);
	}


	/**
	 * Gets the cached articles from database.
	 *
	 * @return {Promise<NewsCache>}
	 * @abstract
	 */
	async retrieveCache() {
		let result = this.client.provider.get ('global', this.name, '{ "hash" : null, "articles" : null }');
		result = JSON.parse (result);

		if (result.articles) {
			let decompressed = Buffer.from (result.articles, 'binary');
			decompressed = brotliDecompressSync (decompressed);
			decompressed = JSON.parse (decompressed);
			result.articles = decompressed;
		}

		return result;
	}


	/**
	 * Caches the news article(s) into database.
	 *
	 * Overwritten existing cache.
	 *
	 * @param {NewsArticle|Array<NewsArticle>} article - The news article(s).
	 * @return {Promise<NewsCache>} The cached news.
	 */
	async cache(article) {
		article = isArray (article) ? article : [article];
		const result = {};

		Winston.debug('cache()', { class: this.constructor.name, status: 'start' });
		article = JSON.stringify (article);
		const hash = createHash ('md5').update (article).digest ('hex');
		article = brotliCompressSync (article);
		article = article.toString ('binary');
		result.hash = hash;
		result.articles = article;
		await this.client.provider.set ('global', this.name, JSON.stringify (result));
		Winston.verbose('cache()', { class: this.constructor.name, status: 'done' });

		return result;
	}


	/**
	 * Checks whether cached articles are outdated.
	 *
	 * @return {Promise<boolean>}
	 */
	async isOutdated() {
		return (await this.retrieveCache()).hash !== createHash ('md5').update (await this.retrieve(true)).digest ('hex');
	}


	/**
	 * Updates news articles.
	 *
	 * @return {Promise<NewsArticle[]>} The new fresh articles.
	 */
	async update() {
		if (!(await this.isOutdated())) {
			Winston.warn('update() Is not outdated.', { class: this.constructor.name, status: 'warn' });
			return [];
		}

		if (this.updating) {
			Winston.warn('update() Another updating is still in progress.', { class: this.constructor.name, status: 'warn' });
			return [];
		}

		Winston.debug('update()', { class: this.constructor.name, status: 'start' });
		this._updating = true;
		// Get the articles.
		const [latest, old] = await Promise.all([ this.retrieve(), this.retrieveCache() ]);
		if (!isArray(latest) || isEmpty(latest)) throw new TypeError(`Failed to retrieve latest ${this.name} articles.`);

		// Stores to database.
		await this.cache(latest);
		this._updating = false;
		Winston.verbose('update()', { class: this.constructor.name, status: 'done' });

		return differenceWith (latest, old.articles, Comparator.Article.isEquals);
	}


	/**
	 * Checks whether this guild/channel is subscribing to news subscription feed.
	 * @param {Guild|string} guild - The guild where channel is located.
	 * @param {Channel|string} [channel] - The guild channel.
	 * @return {Promise<boolean>}
	 */
	async hasSubscriber(guild, channel) {
		if (guild instanceof Guild) { guild = guild.id; }
		if (channel instanceof Channel) { channel = channel.id; }

		const subscribers = await this.getSubscriber (guild);
		return (isEmpty(channel) && !isEmpty(subscribers)) || (!isEmpty(channel) && indexOf(subscribers, channel) !== -1);
	}


	/**
	 * Gets all channels from news subscription feed.
	 * @param {Guild|string} guild - The guild where channel is located.
	 * @return {Promise<Array<string>>}
	 */
	async getSubscriber(guild) {
		if (guild instanceof Guild) { guild = guild.id; }
		return JSON.parse (this.client.provider.get (guild, this.name, '[]'));
	}


	/**
	 * Sets channel(s) into news subscription feed.
	 * @param {Guild|string} guild - The guild where channel is located.
	 * @param {Channel|string|Array<Channel>|Array<string>} channel - The guild channel.
	 * @return {Promise<void>}
	 */
	async setSubscriber(guild, channel) {
		channel = isArray(channel) ? channel : [channel];
		const newsName = this.name;
		let guildName = guild;

		if (guild instanceof Guild) {
			guildName = guild.name;
			guild = guild.id;
		}

		Winston.debug('setSubscriber()', { class: this.constructor.name, status: 'start', guild: guildName, channel: channel });
		await this.client.provider.set (guild, newsName, JSON.stringify(channel));
		Winston.verbose('setSubscriber()', { class: this.constructor.name, status: 'done', guild: guildName, channel: channel });
	}


	/**
	 * Adds a channel into news subscription feed.
	 * @param {Guild|string} guild - The guild where channel is located.
	 * @param {Channel|string} channel - The guild channel.
	 * @return {Promise<boolean>}
	 */
	async addSubscriber(guild, channel) {
		let guildName = guild;
		let channelName = channel;

		if (guild instanceof Guild) {
			guildName = guild.name;
			guild = guild.id;
		}
		if (channel instanceof Channel) {
			channelName = channel.name;
			channel = channel.id;
		}

		Winston.debug('addSubscriber()', { class: this.constructor.name, status: 'start', guild: guildName, channel: channelName });
		const self = this;
		return self.hasSubscriber (guild, channel)
			.then((hasIt) => {
				if (hasIt) {
					Winston.warn('addSubscriber() Is already a subscriber.', { class: this.constructor.name, status: 'warn', guild: guildName, channel: channelName });
					return false;
				}
				return true;
			})
			.then(async (isOk) => {
				if (!isOk) return false;
				const subscribers = await self.getSubscriber (guild);
				subscribers.push (channel);
				await self.setSubscriber (guild, subscribers);
				Winston.verbose('addSubscriber()', { class: this.constructor.name, status: 'done', guild: guildName, channel: channelName });
				return true;
			});
	}


	/**
	 * Deletes a channel from news subscription feed.
	 * @param {Guild|string} guild - The guild where channel is located.
	 * @param {Channel|string} channel - The guild channel.
	 * @return {Promise<boolean>}
	 */
	async deleteSubscriber(guild, channel) {
		let guildName = guild;
		let channelName = channel;

		if (guild instanceof Guild) {
			guildName = guild.name;
			guild = guild.id;
		}
		if (channel instanceof Channel) {
			channelName = channel.name;
			channel = channel.id;
		}

		Winston.debug('deleteSubscriber()', { class: this.constructor.name, status: 'start', guild: guildName, channel: channelName });
		const self = this;
		return self.hasSubscriber(guild, channel)
			.then((hasIt) => {
				if (!hasIt) {
					Winston.warn('deleteSubscriber() Is not a subscriber.', { class: this.constructor.name, status: 'warn', guild: guildName, channel: channelName });
					return false;
				}
				return true;
			})
			.then(async (isOk) => {
				if (!isOk) return false;
				const subscribers = await self.getSubscriber (guild);
				pull (subscribers, channel);
				await self.setSubscriber (guild, subscribers);
				Winston.verbose('deleteSubscriber()', { class: this.constructor.name, status: 'done', guild: guildName, channel: channelName });
				return true;
			});
	}


	/**
	 * Send a news message to the channel.
	 *
	 * If failed to send message with rich embed format, then fallbacks to text format.
	 *
	 * @param {TextChannel} channel The channel to send for.
	 * @param {NewsArticle} article The news article to send.
	 * @param {?MessageOptions|?Attachment|?RichEmbed} [options] Options for the message, can also be just a RichEmbed or Attachment.
	 * @return {Promise<(Message|Array<Message>)>}
	 */
	async send(channel, article, options) {
		if (channel instanceof TextChannel == false) throw new TypeError('Invalid text channel.');
		const self = this;
		return channel.send (self.toRichEmbedMessage(article), options)
			.catch((er) =>{
				Winston.error(er.stack, { class: this.constructor.name, status: 'exception' });
				return channel.send (self.toTextMessage(article), options);
			});
	}


	/**
	 * Returns the news in plain text message forms.
	 * @param {NewsArticle} article The news article to format.
	 * @return {Array<string>}
	 * @abstract
	 */
	toTextMessage(article) {
		return `\`\`\`${article.date}\`\`\`
**${article.title}**
${article.url}`;
	}


	/**
	 * Returns the news in Discord rich embed message forms.
	 * @param {NewsArticle} article The news article to format.
	 * @return {RichEmbed}
	 */
	toRichEmbedMessage(article) {
		return new RichEmbed()
			.setColor('#0099ff')
			.setAuthor(article.author, article.icon, article.url)
			.setTitle(article.title)
			.setThumbnail(article.thumbnail)
			.setDescription(truncate(article.content, { length: process.env.NEWS_MAX_DESCRIPTION_LENGTH }))
			.addField ('Read more...', article.url)
			.setURL (article.url)
			.setFooter(article.author, article.icon)
			.setImage(article.thumbnail)
			.setTimestamp(article.date);
	}
}


module.exports = News;
