module.exports = {
	XBBCode: require('./base'),
	MarkdownXBBCode: require('./markdown'),
	SteamMarkdownXBBCode: require('./steam'),
};
