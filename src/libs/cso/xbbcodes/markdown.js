/*
*
*	Libraries.
*
*/
const XBBCode = require('./base');
const { replace } = require('lodash');
const TurndownService = require('turndown');


/**
 * BBCode Markdown Parser.
 */
class MarkdownXBBCode extends XBBCode {

	/**
	 * Parses the given string.
	 * @param {string} text The string to parse.
	 * @param {ParserOptions} config Parsing option(s).
	 * @return {MarkdownParserResult}
	 */
	static process(text, config) {
		const result = super.process(text, config);
		result.html = replace (result.html, /\n/g, '<br/>');
		result.markdown = (new TurndownService ()).turndown(result.html);
		return result;
	}
}


module.exports = MarkdownXBBCode;
