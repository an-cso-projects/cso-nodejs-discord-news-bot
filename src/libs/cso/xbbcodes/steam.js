/*
*
*	Libraries.
*
*/
const MarkdownXBBCode = require('./markdown');
const { forEach, split } = require('lodash');


/**
 * Steam BBCode Parser.
 */
class SteamMarkdownXBBCode extends MarkdownXBBCode {

	static init() {
		super.init();

		this.addTags (this.createSimpleTag('b'));
		this.addTags (this.createSimpleTag('i'));
		this.addTags (this.createSimpleTag('u'));
		for (let i = 1; i <= 6; i++) {
			this.addTags ({
				[`h${i}`]: {
					openTag: function() {
						return '<h1>';
					},
					closeTag: function() {
						return '</h1>';
					},
				},
			});
		}
		this.addTags ({
			strike: {
				openTag: function() {
					return '<s>';
				},
				closeTag: function() {
					return '</s>';
				},
			},
		});
		this.addTags ({
			img: {
				openTag: function(_params, content) {
					return content;
				},
				closeTag: function() {
					return '';
				},
				displayContent: false,
			},
		});
		this.addTags ({
			quote: {
				openTag: function(params, content) {
					let result = params ? `> <i>Originally posted by <b>${params}:</b></i>\n` : '';
					forEach (split (content, '\n'), (line) => {
						result += '> ' + line + '\n';
					});
					return result;
				},
				closeTag: function() {
					return '';
				},
				displayContent: false,
			},
		});
		this.addTags ({
			previewyoutube: {
				openTag: function(params) {
					return `https://www.youtube.com/watch?v=${split (params, ';')[0]}`;
				},
				closeTag: function() {
					return '';
				},
				displayContent: false,
			},
		});
	}
}


module.exports = SteamMarkdownXBBCode;
